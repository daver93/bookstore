﻿namespace BookStore.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BookStore.Context.BookContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "BookStore.Context.BookContext";
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(BookStore.Context.BookContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            if (!context.Books.ToList().Any())
            {
                context.Books.Add(new Entities.BookEntity
                {
                    Title = "Book 1",
                    Author = "Author 1",
                    Price = 2.90M
                });

                context.Books.Add(new Entities.BookEntity
                {
                    Title = "Book 2",
                    Author = "Author 2",
                    Price = 6.00M
                });

                context.Books.Add(new Entities.BookEntity
                {
                    Title = "Book 3",
                    Author = "Author 3",
                    Price = 7.90M
                });
            }
        }
    }
}
