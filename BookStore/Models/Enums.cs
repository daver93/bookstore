﻿using System.Text.Json.Serialization;

namespace BookStore.Models
{
    /// <summary>
    /// This class has all the available enums of the project
    /// </summary>
    public class Enums
    {
        /// <summary>
        /// By which property to sort
        /// </summary>
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public enum SortingProperty
        {
            Title,
            Author,
            Id
        }
    }
}
