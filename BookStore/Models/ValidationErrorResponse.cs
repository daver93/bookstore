﻿using System.Collections.Generic;

namespace BookStore.Models
{
    public class ValidationErrorResponse
    {
        public List<string> Errors { get; set; } = null;

        public ValidationErrorResponse(List<string> errors)
        {
            Errors = errors;
        }
    }
}
