﻿namespace BookStore.CommonUtils
{
    public static class CommonUtils
    {
        public static string TrimmedOrNull(this string value)
        {
            return (value == null) ? null : 
                !string.IsNullOrWhiteSpace(value) ? value.Trim() : 
                null;
        }
    }
}
