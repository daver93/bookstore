﻿using BookStore.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BookStore.Models.Enums;

namespace BookStore.Repository
{
    public interface IBooksRepository
    {
        public Task<bool> AddBook(BookEntity book);

        public Task<List<BookEntity>> GetAll(SortingProperty sorting);

        public Task<BookEntity> GetBookById(long id);

        public Task<bool> UpdateBook(BookEntity book);

        public Task<bool> DeleteBook(long id);
    }
}
