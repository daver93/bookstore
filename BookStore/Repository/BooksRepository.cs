﻿using BookStore.Context;
using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using static BookStore.Models.Enums;

namespace BookStore.Repository
{
    public class BooksRepository : IBooksRepository
    {
        public async Task<bool> AddBook(BookEntity book)
        {
            try
            {
                using (BookContext context = new BookContext())
                {
                    context.Books.Add(book);

                    if (await context.SaveChangesAsync() < 0)
                    {
                        throw new Exception("Book failed to be saved into db");
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<BookEntity>> GetAll(SortingProperty sorting)
        {
            try
            {
                using (BookContext context = new BookContext())
                {
                    var books = await context.Books.ToListAsync();

                    switch (sorting)
                    {
                        case SortingProperty.Id:
                            books = books.OrderBy(o => o.Id).ToList();
                            break;
                        case SortingProperty.Author:
                            books = books.OrderBy(o => o.Author).ToList();
                            break;
                        case SortingProperty.Title:
                        default:
                            books = books.OrderBy(o => o.Title).ToList();
                            break;
                    }

                    return books;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }

        public async Task<BookEntity> GetBookById(long id)
        {
            try
            {
                using (BookContext context = new BookContext())
                {
                    return await context.Books.SingleOrDefaultAsync(o => o.Id == id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> UpdateBook(BookEntity book)
        {
            try
            {
                using (BookContext context = new BookContext())
                {
                    var existingBook = await GetBookById(book.Id);

                    if (existingBook != null)
                    {
                        existingBook.Title = book.Title;
                        existingBook.Author = book.Author;
                        existingBook.Price = book.Price;

                        context.Entry(existingBook).State = EntityState.Modified;

                        if (await context.SaveChangesAsync() < 0)
                        {
                            throw new Exception("Book failed to be saved into db");
                        }

                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> DeleteBook(long id)
        {
            try
            {
                using (BookContext context = new BookContext())
                {
                    var book = await context.Books.SingleOrDefaultAsync(o => o.Id == id);

                    if (book != null)
                    {
                        context.Books.Remove(book);

                        if (await context.SaveChangesAsync() < 0)
                        {
                            throw new Exception("Book failed to be deleted");
                        }

                        return true;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
