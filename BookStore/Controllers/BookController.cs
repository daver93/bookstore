﻿using BookStore.BusinessLogic;
using BookStore.CommonUtils;
using BookStore.Entities;
using BookStore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BookStore.Models.Enums;

namespace BookStore.Controllers
{
    [Controller]
    [Route("Books")]
    public class BookController : Controller
    {
        public IBookService _bookService;

        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }

        /// <summary>
        /// Creates a new book
        /// </summary>
        /// <param name="request"></param>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(long))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationErrorResponse))]
        public async Task<IActionResult> CreateBook([FromBody] BookModel request)
        {
            try
            {
                #region Validation Area

                request.Title = request.Title.TrimmedOrNull();
                request.Author = request.Author.TrimmedOrNull();
                request.Price = request.Price;

                List<string> errors = new List<string>();

                if (request.Title == null)
                {
                    errors.Add($"{nameof(request.Title)} is required");
                }
                if (request.Author == null)
                {
                    errors.Add($"{nameof(request.Author)} is required");
                }
                if (request.Price == decimal.MinValue)
                {
                    errors.Add($"{nameof(request.Price)} is not valid");
                }

                if (errors.Count > 0)
                {
                    return BadRequest(new ValidationErrorResponse(errors));
                }

                #endregion Validation Area

                long response = await _bookService.AddBook(request);

                if (response <= -1)
                {
                    return BadRequest();
                }

                return new ObjectResult(response) { StatusCode = StatusCodes.Status201Created };
            }
            catch (Exception e)
            {
                return BadRequest(); // More user friendly that an exception
            }
        }

        /// <summary>
        /// Returns a list of books. Sorted by title by default.
        /// </summary>
        /// <param name="sortBy"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<BookEntity>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetBooksAll(SortingProperty sortBy)
        {
            try
            {
                List<BookEntity> response = null;
                var books = await _bookService.GetAllBooks(sortBy);

                if (books == null)
                {
                    return NotFound();
                }

                response = books;

                return new OkObjectResult(response);
            }
            catch (Exception e)
            {
                return NotFound(); // More user friendly that an exception
            }
        }

        /// <summary>
        /// Gets a book by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BookModel))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetBookById(int id)
        {
            try
            {
                BookModel response = new BookModel();

                var book = await _bookService.GetBookById(id);

                if (book == null)
                {
                    return NotFound("Book not found");
                }

                response.Title = book.Title;
                response.Author = book.Author;
                response.Price = book.Price;

                return new OkObjectResult(response);
            }
            catch (Exception e)
            {
                return NotFound(); // More user friendly that an exception
            }
        }

        /// <summary>
        /// Update an existing book
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationErrorResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateBook(int id, [FromBody] BookModel request)
        {
            try
            {
                #region Validation Area

                request.Title = request.Title.TrimmedOrNull();
                request.Author = request.Author.TrimmedOrNull();
                request.Price = request.Price;

                List<string> errors = new List<string>();

                if (id <= 0)
                {
                    errors.Add($"{nameof(id)} is not valid. Value should be greater than 0");
                }
                if (request.Title == null)
                {
                    errors.Add($"{nameof(request.Title)} is required");
                }
                if (request.Author == null)
                {
                    errors.Add($"{nameof(request.Author)} is required");
                }
                if (request.Price == decimal.MinValue)
                {
                    errors.Add($"{nameof(request.Price)} is not valid");
                }

                if (errors.Count > 0)
                {
                    return BadRequest(new ValidationErrorResponse(errors));
                }

                #endregion Validation Area

                BookEntity book = new BookEntity
                {
                    Id = id,
                    Title = request.Title,
                    Author = request.Author,
                    Price = request.Price
                };

                var updated = await _bookService.UpdateBook(book);

                if (!updated)
                {
                    return NotFound("Book not found");
                }

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(); // More user friendly that an exception
            }
        }

        /// <summary>
        /// Deletes a book by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteBook(int id)
        {
            try
            {
                var book = await _bookService.DeleteBook(id);
                return book ? Ok("Success") : NotFound("Book not found");
            }
            catch (Exception e)
            {
                return NotFound(); // More user friendly that an exception
            }
        }
    }
}
