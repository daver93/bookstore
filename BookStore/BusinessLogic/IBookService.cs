﻿using BookStore.Entities;
using BookStore.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BookStore.Models.Enums;

namespace BookStore.BusinessLogic
{
    public interface IBookService
    {
        Task<long> AddBook(BookModel request);

        Task<List<BookEntity>> GetAllBooks(SortingProperty sortBy);

        Task<BookEntity> GetBookById(long id);

        Task<bool> UpdateBook(BookEntity book);

        Task<bool> DeleteBook(long id);
    }
}
