﻿using BookStore.Entities;
using BookStore.Models;
using BookStore.Repository;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BookStore.Models.Enums;

namespace BookStore.BusinessLogic
{
    public class BookService : IBookService
    {
        private readonly IBooksRepository _booksRepository;

        public BookService(IBooksRepository booksRepository)
        {
            _booksRepository = booksRepository;
        }

        public async Task<long> AddBook(BookModel request)
        {
            try
            {
                if (request != null)
                {
                    BookEntity book = new BookEntity
                    {
                        Title = request.Title,
                        Author = request.Author,
                        Price = request.Price
                    };

                    await _booksRepository.AddBook(book);

                    return book.Id;
                }

                return -1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<BookEntity>> GetAllBooks(SortingProperty sortBy)
        {
            try
            {
                return await _booksRepository.GetAll(sortBy);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<BookEntity> GetBookById(long id)
        {
            try
            {
                return await _booksRepository.GetBookById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> UpdateBook(BookEntity book)
        {
            try
            {
                if (await GetBookById(book.Id) == null)
                {
                    return false;
                }

                return await _booksRepository.UpdateBook(book);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> DeleteBook(long id)
        {
            try
            {
                var book = await GetBookById(id);

                if (book != null)
                {
                    return await _booksRepository.DeleteBook(book.Id);
                }

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
