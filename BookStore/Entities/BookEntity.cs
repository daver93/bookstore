﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.Entities
{
    public class BookEntity
    {
        [Key]
        public long Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Author { get; set; }

        [Required]
        public decimal Price { get; set; }
    }
}
