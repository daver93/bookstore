﻿using BookStore.Entities;
using System.Data.Entity;

namespace BookStore.Context
{
    public class BookContext : DbContext
    {
        public virtual DbSet<BookEntity> Books { get; set; }
    }
}
