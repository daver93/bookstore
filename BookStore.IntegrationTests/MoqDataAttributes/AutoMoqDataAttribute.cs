﻿using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;
using BookStore.IntegrationTests.Customizations;

namespace BookStore.IntegrationTests.MoqDataAttributes
{
    public class AutoMoqDataAttribute : AutoDataAttribute
    {
        public AutoMoqDataAttribute()
            : base(() =>
            {
                var fixture = new Fixture();

                fixture.Customize(new AutoMoqCustomization() { ConfigureMembers = true });
                fixture.Customize(new BookCustomization());

                return fixture;
            })
        {

        }
    }

}
