using BookStore.Entities;
using BookStore.IntegrationTests.MoqDataAttributes;
using BookStore.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Xunit;

namespace BookStore.IntegrationTests
{
    public class IntegrationTest
    {
        private readonly HttpClient _httpClient;
        private readonly TestServer _server;

        public IntegrationTest()
        {
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _httpClient = _server.CreateClient();
        }

        [Theory, AutoMoqData]
        public async Task CreateBook(BookModel book)
        {
            var createBook = CreateBookHelper(book);
            var response = await _httpClient.PostAsync("/Books", createBook);

            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            var stringResult = await response.Content.ReadAsStringAsync();

            var deserializeResult = JsonConvert.DeserializeObject<long>(stringResult);
            Assert.True(deserializeResult > 0);
        }

        [Fact]
        public async Task GetAllBooks()
        {
            var jsonRequest = JsonConvert.SerializeObject(Enums.SortingProperty.Title);
            var buffer = System.Text.Encoding.UTF8.GetBytes(jsonRequest);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await _httpClient.GetAsync($"/Books?SortBy={Enums.SortingProperty.Title}");
            var stringResult = await response.Content.ReadAsStringAsync();

            var deserializeResult = JsonConvert.DeserializeObject<List<BookEntity>>(stringResult);
            Assert.True(deserializeResult.Count > 0);
        }

        [Theory]
        [InlineData(2)]
        public async Task GetBookById(int id)
        {
            var response = await _httpClient.GetAsync($"/Books/{id}");
            
            Assert.True(response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.NotFound);
            
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var stringResult = await response.Content.ReadAsStringAsync();
                var deserializeResult = JsonConvert.DeserializeObject<BookModel>(stringResult);

                Assert.NotNull(deserializeResult);
            }
        }

        [Theory, AutoMoqData]
        public async Task UpdateBookById(BookModel book)
        {
            var createBook = CreateBookHelper(book);
            var createResponse = await _httpClient.PostAsync("/Books", createBook);

            var stringCreateResult = await createResponse.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<long>(stringCreateResult);


            book.Title = "Updated Title";
            var request = CreateBookHelper(book);

            var response = await _httpClient.PutAsync($"/Books/{id}", request);

            Assert.True(response.IsSuccessStatusCode);
        }

        [Theory]
        [InlineData(2)]
        public async Task DeleteBookById(int id)
        {
            var getResponse = await _httpClient.GetAsync($"/Books/{id}");

            var response = await _httpClient.DeleteAsync($"/Books/{id}");

            if (getResponse.IsSuccessStatusCode)
            {
                Assert.True(response.IsSuccessStatusCode);
            }
            else
            {
                Assert.False(response.IsSuccessStatusCode);
            }
        }

        private ByteArrayContent CreateBookHelper(BookModel book)
        {
            var request = CreateUpdateRequest(book);

            var jsonRequest = JsonConvert.SerializeObject(request);
            var buffer = System.Text.Encoding.UTF8.GetBytes(jsonRequest);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return byteContent;
        }

        private BookModel CreateUpdateRequest(BookModel book)
        {
            return new BookModel
            {
                Title = book.Title,
                Author = book.Author,
                Price = book.Price
            };
        }
    }
}
