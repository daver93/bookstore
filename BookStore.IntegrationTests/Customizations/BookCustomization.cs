﻿using AutoFixture;
using BookStore.Entities;

namespace BookStore.IntegrationTests.Customizations
{
    public class BookCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Register(() =>
                new BookEntity () 
                {
                    Id = 1,
                    Title = "The Bazaar of Bad Dreams",
                    Author = "Stephen King",
                    Price = 1.90M
                }
            );

            var book = fixture.Create<BookEntity>();
        }
    }
}
